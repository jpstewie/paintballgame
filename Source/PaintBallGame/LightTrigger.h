#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LightTrigger.generated.h"

class UPointLightComponent;
class USphereComponent;

UCLASS()
class PAINTBALLGAME_API ALightTrigger : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALightTrigger();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//point light component
	UPROPERTY(VisibleAnywhere, Category = "Light Switch")
		class UPointLightComponent* PointLight;

	//sphere component
	UPROPERTY(VisibleAnywhere, Category = "Light Switch")
		class USphereComponent* LightSphere;

	//light intensity
	UPROPERTY(VisibleAnywhere, Category = "Light Switch")
		float LightIntensity;

	//Light colors
	UPROPERTY(VisibleAnywhere, Category = "Light Switch")
		FLinearColor LightOn;

	UPROPERTY(VisibleAnywhere, Category = "Light Switch")
		FLinearColor LightOff;

	//when character overlap begins
	UFUNCTION()
		void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	//when character overlap ends
	UFUNCTION()
		void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};
