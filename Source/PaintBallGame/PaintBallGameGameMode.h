// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PaintBallGameGameMode.generated.h"

UCLASS(minimalapi)
class APaintBallGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APaintBallGameGameMode();
};



