#pragma once

#include "ProceduralMeshComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BoxStuff.generated.h"


UCLASS()
class PAINTBALLGAME_API ABoxStuff : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABoxStuff();
	virtual void PostActorCreated() override;
	virtual void PostLoad() override;
	virtual void CreateSquare();

private:
	UPROPERTY(VisibleAnywhere)
		UProceduralMeshComponent * mesh;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
